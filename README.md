## Contents
- `assigntools`: the cloned repo with helper methods provided by the course
- `data`: folder containing all sorts of data other than the code to generate it.
    - `generated_data`: excel files containing the datasets generated in `generate_datasets.ipynb`
    - `prior_content`: excel files containing the weaker-stronger adjective pairs selected for this experiment, as well as filtered SNLI and MNLI datasets. The datasets were extracted from the `datasets` python module, and filtered in `generate_datasets.ipynb` to only those sentences containing the selected adjectives.
    - `results`: excel files containing the classification labels of RoBERTa and DeBERTa, as well as the gold label, for all sentences in the datasets. Also contains the confusion matrix and performance metrics for each dataset.
    - `vocab and pairs check`: excel files containing information about the occurrences of certain adjectives in the dataset sentences.
- `generate_datasets.ipynb`: can be used to generate the files in `prior_content` and `generated_data` folders.
- `LoLa_Generation_project.pdf`: the research paper accompanying the code.
- `requirements.txt`: can be used to create a conda environment satisfying all requirements for running the code.
- `Results.ipynb`: runs RoBERTa and DeBERTa on the classification test of the datasets and generates files in `results` folder.
- `Vocabulary and pairs check.ipynb`: can be used to generate the files in `vocab and pairs check` folder.


## Running the code
- `Vocabulary and pairs check.ipynb` can be executed code block by block to generate the files in `vocab and pairs check` folder. However, these files are already included in the repo. 
- In `generate_datasets.ipynb`, each code cell can be run individually to generate a dataset of choice. However, the first two cells should always be run first. The code needs not be run, however, since these dataset files are already included in the  `prior_content` and `generated_data` folders. 
- To run RoBERTa and DeBERTa for the classification task, one can run `Results.ipynb`. First, the first three code cells should be executed. Then, any of the datasets can be used for model evaluation by running a code cell of choice. The last code cell is used to generate the confusion matrix and performance measures for each dataset and model. All resulting files are already included in the `results` folder
